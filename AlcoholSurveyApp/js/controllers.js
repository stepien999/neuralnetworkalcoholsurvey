angular.module('AlcoholSurveyApp.controllers',[]).controller('SurveyCreateController',function($scope,$state,$stateParams,Survey){

    $scope.survey=new Survey();

    $scope.addSurvey=function(){
        $scope.survey.$save(function(){
            var result;
            if ($scope.survey.neuroResult == 1) {
                result = "Nigdy";
            }
            if ($scope.survey.neuroResult == 0) {
                result = "3x dziennie";
            }
            if ($scope.survey.neuroResult == 0.1) {
                result = "2x dziennie";
            }
            if ($scope.survey.neuroResult == 0.2) {
                result = "Codziennie";
            }
            if ($scope.survey.neuroResult == 0.3) {
                result = "Prawie codziennie";
            }
            if ($scope.survey.neuroResult == 0.4) {
                result = "2-3x tygodniowo";
            }
            if ($scope.survey.neuroResult == 0.5) {
                result = "1-2x tygodniowo";
            }
            if ($scope.survey.neuroResult == 0.6) {
                result = "2-3x miesięcznie";
            }
            if ($scope.survey.neuroResult == 0.7) {
                result = "Raz na miesiąc";
            }
            if ($scope.survey.neuroResult == 0.8) {
                result = "Więcej niż raz na rok";
            }
            if ($scope.survey.neuroResult == 0.9) {
                result = "Mniej niż raz na rok";
            }
            $("#surveyModal").modal({show: true});
            $("#modalBody").html ("Twój wynik: " + result);
        });

    }
});
