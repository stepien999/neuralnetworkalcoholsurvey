angular.module('AlcoholSurveyApp.services',[]).factory('Survey',function($resource){
    return $resource('http://localhost:8080/submitSurvey', {}, {
    save: {
      method: 'PUT' // this method issues a PUT request
  }});
}).service('popupService',function($window){
    this.showPopup=function(message){
        return $window.confirm(message);
    }
});
