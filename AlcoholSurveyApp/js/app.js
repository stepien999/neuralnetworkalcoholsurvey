angular.module('AlcoholSurveyApp',['ui.router','ngResource','AlcoholSurveyApp.controllers','AlcoholSurveyApp.services']);

angular.module('AlcoholSurveyApp').config(function($stateProvider,$httpProvider){
    $stateProvider.state('newSurvey',{
        url:'/survey/new',
        templateUrl:'partials/survey-add.html',
        controller:'SurveyCreateController'
    });
}).run(function($state){
    $state.go('newSurvey');
});
