package neuro.neuralnet;

import java.io.IOException;
import java.io.Serializable;

import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;

public class NeuralNet implements Serializable {

    public static final String FILE_PATH = "multiLayerPerceptron.nnet";
    private static NeuralNet instance = null;
    private NeuralNetwork neuralNetwork;

    private NeuralNet() throws IOException {
        init();
    }

    public static NeuralNet getInstance() throws IOException {
        if (instance == null) {
            instance = new NeuralNet();
        }
        return instance;
    }

    public void init() throws IOException {
        neuralNetwork = NeuralNetwork.createFromFile(FILE_PATH);
    }

    public double testNeuralNetwork(DataSet testSet) {

        double networkOutput = -1;

        for (DataSetRow dataRow : testSet.getRows()) {
            neuralNetwork.setInput(dataRow.getInput());
            neuralNetwork.calculate();
            networkOutput = neuralNetwork.getOutput()[0];
        }

        if (networkOutput == -1)
            System.out.println("Something went wrong");

        return networkOutput;
    }

    // private void learnNetwork() {
    // String smallDataFile = "trainingData.csv";
    //
    // DataSet smallData1 = parseDataForNeuralNet(smallDataFile);
    //
    // multiLayerPerceptron = new MultiLayerPerceptron(TransferFunctionType.SIGMOID, 10, 7, 1);
    // BackPropagation backPropagation = new BackPropagation();
    // backPropagation.setMaxIterations(500);
    // backPropagation.setMaxError(0.01);
    //
    // multiLayerPerceptron.learn(smallData1, backPropagation);
    // multiLayerPerceptron.save("multiLayerPerceptron.nnet");
    // }
    //
    // public DataSet parseDataForNeuralNet(String filePath) {
    // DataSet trainingSet = new DataSet(10, 1);
    // String lineFromFile;
    // double[] doubleValues;
    //
    // try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
    // while ((lineFromFile = br.readLine()) != null) {
    // String[] valuesFromLine = lineFromFile.split(";");
    // doubleValues = Arrays.stream(valuesFromLine)
    // .mapToDouble(Double::parseDouble)
    // .toArray();
    // trainingSet.addRow(Arrays.copyOfRange(doubleValues, 0, 10),
    // Arrays.copyOfRange(doubleValues, 10, doubleValues.length));
    // }
    // } catch (IOException e) {
    // e.printStackTrace();
    // }
    // return trainingSet;
    // }
}