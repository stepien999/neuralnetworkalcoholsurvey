package neuro.web;/*
 * Copyright 2012-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import neuro.model.ResponseTO;
import neuro.model.SurveyTO;
import neuro.service.NeuroService;


@Controller
public class NeuroController {

    @Autowired
    private NeuroService neuroService;

    @GetMapping("/")
    @ResponseBody
    public String helloWorld() {
        return "NeuroService";
    }

    @SuppressWarnings({
            "rawtypes"
    })
    @RequestMapping(
            value = "/submitSurvey",
            method = RequestMethod.PUT,
            produces = "application/json; charset=UTF-8",
            consumes = "application/json; charset=UTF-8")
    @ResponseBody
    public ResponseTO submitSurvey(@RequestBody SurveyTO surveyTO) {
        return this.neuroService.submitSurvey(surveyTO);
    }
}
