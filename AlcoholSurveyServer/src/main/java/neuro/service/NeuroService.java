package neuro.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;

import org.neuroph.core.data.DataSet;
import org.springframework.stereotype.Component;

import neuro.model.ResponseTO;
import neuro.model.SurveyTO;
import neuro.neuralnet.NeuralNet;

@Component
public class NeuroService {

    public static final int INPUT_SIZE = 10;
    private NeuralNet neuralNet = NeuralNet.getInstance();

    public NeuroService() throws IOException {
    }

    public ResponseTO submitSurvey(SurveyTO surveyTO) {
        ResponseTO response = new ResponseTO();
        DataSet testSet = new DataSet(INPUT_SIZE, 1);

        try {
            double[] surveySet = prepareSurveySet(surveyTO);

            testSet.addRow(Arrays.copyOfRange(surveySet, 0, INPUT_SIZE),
                    Arrays.copyOfRange(surveySet, INPUT_SIZE, surveySet.length));

            double neuroResult = neuralNet.testNeuralNetwork(testSet);
            response.setNeuroResult(round(neuroResult));
        } catch (Exception e) {
            response.setMessage("Something went wrong");
            e.printStackTrace();
        }

        return response;
    }

    private double[] prepareSurveySet(SurveyTO surveyTO) {
        double[] surveySet = new double[11];
        surveySet[0] = surveyTO.getSex();
        surveySet[1] = surveyTO.getOrigin();
        surveySet[2] = surveyTO.getAge();
        surveySet[3] = surveyTO.getEducation();
        surveySet[4] = surveyTO.getReligion();
        surveySet[5] = surveyTO.getEmployment();
        surveySet[6] = surveyTO.getIncome();
        surveySet[7] = surveyTO.getWeight();
        surveySet[8] = surveyTO.getMaritalStatus();
        surveySet[9] = surveyTO.getSexualPreference();
        surveySet[10] = 0;
        return surveySet;
    }

    private static double round(double value) {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(1, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
