package neuro.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class SurveyTO {
    private double age;
    private double education;
    private double employment;
    private double income;
    private double maritalStatus;
    private double origin;
    private double religion;
    private double sex;
    private double sexualPreference;
    private double weight;
}

