1. Uruchomienie serwera:
- wywołać java -jar alconeuron-server.jar (ważne by w tej samej lokalizacji znajdował się plik multiLayerPerceptron.nnet)

2. Uruchomienie aplikacji:
- zainstalować narzędzie serve https://www.npmjs.com/package/serve
- bedąc w katalogu z aplikacją w wierszu poleceń wystarczy wpisać serve, co spowoduje jej uruchomienie na domyślnym porcie 3000
- urchomić przeglądarkę Google Chrome w trybie bez zabezpieczeń (problemy z CORSem) np.: chrome.exe --user-data-dir="C:/Chrome dev session" --disable-web-security 
